import $ from 'jquery';
import debounce from 'lodash.debounce';

const scrollHandler = (event, triggerPoint, $navBar) => {
			const currentY = event.currentTarget.scrollY

			if (currentY >= triggerPoint) {
				$navBar.addClass('sticky-visible')

			} else {
				$navBar.removeClass('sticky-visible')
			}
		}

export default () => {

	$(document).ready(() => {
		const triggerPoint = $('#sticky-trigger').offset().top
		const $navBar = $('#sticky-cta')


		$(window).on("scroll", debounce((event) => {
			scrollHandler(event, triggerPoint, $navBar)
		}, 50))

	})
	
}