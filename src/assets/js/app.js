import 'babel-polyfill';
import $ from 'jquery';
import 'what-input';

import stickyCTA from './lib/sticky-cta'

window.$ = $;

import Foundation from 'foundation-sites';
// If you want to pick and choose which modules to include, comment out the above and uncomment
// the line below
//import './lib/foundation-explicit-pieces';

//ZURB Foundation
$(document).foundation();

//Sticky CTA
 stickyCTA();